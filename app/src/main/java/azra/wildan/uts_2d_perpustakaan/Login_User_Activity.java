package azra.wildan.uts_2d_perpustakaan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Login_User_Activity extends AppCompatActivity {
    Button btnLogin2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_user);

        btnLogin2 = (Button)findViewById(R.id.btnLogin2);

        btnLogin2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent MenuUser = new Intent(getApplicationContext(),Menu_Utama_User.class);
                startActivity(MenuUser);
            }
        });
    }
}
