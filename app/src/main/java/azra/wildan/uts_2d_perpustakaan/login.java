package azra.wildan.uts_2d_perpustakaan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class login extends AppCompatActivity {
    Button btnLoginAdmin, btnLoginUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnLoginAdmin = (Button)findViewById(R.id.btnLoginAdmin);
        btnLoginUser = (Button)findViewById(R.id.btnLoginUser);

        btnLoginAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent LoginAdmin = new Intent(getApplicationContext(),Login_Admin_Activity.class);
                startActivity(LoginAdmin);
            }
        });

        btnLoginUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent LoginUser = new Intent(getApplicationContext(),Login_User_Activity.class);
                startActivity(LoginUser);
            }
        });
    }
}
