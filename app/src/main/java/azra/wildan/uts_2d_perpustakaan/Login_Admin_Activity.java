package azra.wildan.uts_2d_perpustakaan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Login_Admin_Activity extends AppCompatActivity {
    Button btnLogin1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_admin);

        btnLogin1 = (Button)findViewById(R.id.btnLogin1);

        btnLogin1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent MenuAdmin = new Intent(getApplicationContext(),Menu_Utama_Admin.class);
                startActivity(MenuAdmin);
            }
        });
    }
}
